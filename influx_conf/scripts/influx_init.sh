#!/bin/sh

set -e

BUCKET_ID=$(influx bucket list | grep "$DOCKER_INFLUXDB_INIT_BUCKET" | awk '{print $1}')

influx v1 auth create \
  --username ${DOCKER_INFLUXDB_INIT_USERNAME} \
  --password ${DOCKER_INFLUXDB_INIT_PASSWORD} \
  --write-bucket ${BUCKET_ID} \
  --read-bucket ${BUCKET_ID} \
  --org ${DOCKER_INFLUXDB_INIT_ORG}

influx v1 dbrp create \
  --bucket-id ${BUCKET_ID} \
  --db krakend \
  --rp example-rp \
  --default


# influx query to test if data is inserted correctly:
# from(bucket: "bucket-load")
# |>range(start: 20XX-01-01T00:00:00Z,stop:2012-XX-02T00:00:00Z)

influx bucket create -n temp_data
# write to temperature bucket
# please check if header is valid
export HEADER="Temperature|dateTime:2006-01-02T15:04:05,1|double,lat-long|measurement"
influx write -b temp_data -f /data/grid_cell_1.csv --skipHeader=1 --header="$HEADER"
